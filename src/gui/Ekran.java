package gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Ekran extends JPanel implements ActionListener
{
    private JFrame pencere = new JFrame("Sayaç");
    private JLabel labelSaat,labelDakika,labelSaniye,labelSanise;
    private JButton baslat,sifirla,tur;
    private JPanel panel1,panel2,panel3;
    private int saat,dk,saniye,sanise;
    private Timer zaman = new Timer(9,this);

    private DefaultListModel listModel = new DefaultListModel();
    private JScrollPane scrollPane;
    private JList jList;

    public Ekran()
    {
        setLayout(new GridLayout(3,1));
        setBackground(new Color(132, 161, 194));

        panel1 = new JPanel();

        labelSaat = new JLabel();
        labelDakika = new JLabel();
        labelSaniye = new JLabel();
        labelSanise = new JLabel();

        labelSaat.setFont(new Font(null,Font.BOLD,21));
        labelDakika.setFont(new Font(null,Font.BOLD,21));
        labelSaniye.setFont(new Font(null,Font.BOLD,21));
        labelSanise.setFont(new Font(null,Font.BOLD,21));

        sureEkranaYaz(0,0,0,0);

        panel1.add(labelSaat);
        panel1.add(labelDakika);
        panel1.add(labelSaniye);
        panel1.add(labelSanise);

        add(panel1);



        panel2 = new JPanel();

        baslat = new JButton("Baslat");
        baslat.addActionListener(this);

        tur = new JButton("Tur");
        tur.setEnabled(false);
        tur.addActionListener(this);

        sifirla = new JButton("Sıfırla");
        sifirla.addActionListener(this);
        sifirla.setEnabled(false);

        panel2.add(sifirla);
        panel2.add(baslat);
        panel2.add(tur);

        add(panel2);

        panel3 = new JPanel();

        jList = new JList();
        scrollPane = new JScrollPane(jList);
        jList.setModel(listModel);
        jList.setPreferredSize(new Dimension(200, 200));

        //scrollPane.setSize(350,200);

        panel3.add(jList);

        add(panel3);

        pencere.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pencere.setLocation(500,400);
        pencere.setSize(new Dimension(600,400));
        //pencere.setResizable(false);

        pencere.add(this);
        pencere.setVisible(true);
    }

    private void sureEkranaYaz(int saat, int dakika, int saniye, int sanise)
    {
        labelSaat.setText(saat > 9 ? saat+" :" : "0" + saat + " :");
        labelDakika.setText(dakika > 9 ? dakika+" :" : "0" + dakika + " :");
        labelSaniye.setText(saniye > 9 ? saniye+" ." : "0" + saniye + " .");
        labelSanise.setText(sanise > 9 ? sanise + "" : "0" + sanise);
    }


    @Override public void actionPerformed(ActionEvent e)
    {
        if(e.getSource().equals(baslat))
        {
            if(zaman.isRunning())
            {
                System.out.println("durduruldu");
                zaman.stop();
                baslat.setText("Başlat");
                tur.setEnabled(false);
            }
            else
            {

                zaman.start();
                System.out.println("başladı");
                baslat.setText("Durdur");
                tur.setEnabled(true);
            }
            sifirla.setEnabled(true);
        }

        if(e.getSource().equals(sifirla))
        {
            zaman.stop();
            baslat.setText("Başlat");
            tur.setEnabled(false);
            sifirla.setEnabled(false);
            saat=0;
            dk=0;
            saniye=0;
            sanise=0;
            sureEkranaYaz(saat,dk,saniye,sanise);
            listModel.removeAllElements();
        }

        if(e.getSource().equals(tur))
        {
            String tur = "";
            if(saat>0)
            {
                tur+= saat > 9 ? saat : "0" + saat;
                tur+=":";
            }

            if(dk>0)
            {
                tur+=dk > 9 ? dk : "0" + dk;
                tur+=":";
            }
            else if(saat!=0)
            {
                tur+="00:";
            }

            if(saniye>0)
            {
                tur+=saniye > 9 ? saniye : "0" + saniye;
                tur+=".";
            }
            else
            {
                tur+="00.";
            }

            tur+=sanise>9?sanise:"0"+sanise;

            System.out.println(tur);
            listModel.addElement(tur);
        }

        if(zaman.isCoalesce() && zaman.isRunning())
        {
            sanise++;
            if(sanise>99)
            {
                sanise=0;
                saniye++;
            }

            if(saniye>59)
            {
                saniye=0;
                dk++;
            }

            if(dk>59)
            {
                dk=0;
                saat++;
            }

            if(saat>59)
            {
                zaman.stop();
            }
            sureEkranaYaz(saat,dk,saniye,sanise);
        }

    }
}
